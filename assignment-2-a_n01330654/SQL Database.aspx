﻿<%@ Page Title="SQL Database" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SQL Database.aspx.cs" Inherits="assignment_2_a_n01330654.SQL_Database" %>

<asp:Content ContentPlaceHolderID="courseOverview" runat="server">

    <div class="courseOverview">
        <h1>Left Join and right Join in SQL</h1>
        <p>A left join in sql returns rows from the left table and matching rows from the right table. In case the right table doesn't have matching records, the value will be NULL in the result set. On the other hand, Right Join returns rows from the right table and matching rows from the left table. If it doesn't have matching records with the left table, the value will be NULL in the result set. When writing a query with left/right join, if both tables have the same column name(for example: primary keys) include the table's name plus the column name. </p>
    </div>
</asp:Content>

<asp:Content ContentPlaceHolderID="mysampleCode" runat="server">
    <div class="col-md-12">
    <h2>My Sample Code</h2>
    <pre>
-- SAMPLE QUERY FOR LEFT JOIN

SELECT invoice_amount, invoice_description, 
clientlname || ', ' || clientfname as fullname
FROM clients
LEFT JOIN invoices
ON clients.clientid = invoices.clientid
WHERE invoice_amount > 500 or
invoice_description like 'replacement'
ORDER BY invoices.clientid asc;

    </pre>
    <img class="sample-img" src="images/left-join.jpg" alt="image of result set in sql">
    <p>This query will find all of the invoices with invoice amount greater than 500, or have “replacement” in their invoice description. If the invoices doesn’t have an associated client, it will return a value which is NULL. The left table is the clients table while the right table is the invoices table. On my where clause, I use the logical operator "or" to find the invoices that's greater than 500 or invoice description with with the keyword "replacement"</p>
    </div>
</asp:Content>

<asp:Content ContentPlaceHolderID="SampleCode" runat="server">
    <div class="col-md-12 class=">
    <h2>In-Class Example</h2>
    <pre>
--SYNTAX FOR LEFT JOIN

SELECT column_name1, column_name2
FROM table_name
LEFT JOIN table_name1
ON table_name1.column_name = table_name1.column_name  

--SYNTAX FOR RIGHT JOIN

SELECT column_name1, column_name2
FROM table_name
RIGHT JOIN table_name1
ON table_name1.column_name = table_name1.column_name 

--Example Query
SELECT clientlname, invoice_description, invoice_dueby
FROM invoices
RIGHT JOIN clients
ON clients.clientid = invoices.clientid

    </pre>
    <img class="sample-img" src="images/right-join.jpg" alt="image of result set in sql">
    <p>On the example query, We want to get find out which invoices belongs to a client. If the the invoices doesn't belong to a client, it will return as NULL. The left table is the clients table and the right table will be invoices table. </p> 
    <p> This example is  from HTTP 5105 - 02 - Database Design & Development - HTTP-5105-0NA and the instructor Christine Bittle</p>
    </div>
</asp:Content>

<asp:Content ContentPlaceHolderID="usefulLinks" runat="server">
            <h2>Other Resources for this topic </h2>
            <ul class="usefulLinks">
                <li><h3><a href="http://sqlhints.com/2016/10/15/difference-between-left-outer-join-and-right-outer-join-in-sql-server/" target="_blank">Difference of Left and Right Join</a></h3></li>
                <li><h3><a href="https://www.dofactory.com/sql/left-outer-join" target="_blank">Left Join</a></h3></li>
                <li><h3><a href="hhttps://www.dofactory.com/sql/right-outer-join" target="_blank">Right Join</a></h3></li>
            </ul>
</asp:Content>
