﻿<%@ Page Title="Javascript" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Javascript.aspx.cs" Inherits="assignment_2_a_n01330654.Javascript" %>

<asp:Content ContentPlaceHolderID="courseOverview" runat="server">

    <div class="courseOverview">
        <h1>Javascript Objects</h1>
        <p>In Javascript, an object is a collection of properties that keeps related information together. Each property can be a string. The value also contains any data type such as numbers, strings, boolean. Objects are denoted by a pair of curly braces. Don't get confused to arrays, as it used square brackets. To call for an object property you need to use a dot notation. An obect can also have their own functions. these functions inside the object are called "methods"</p>
    </div>
</asp:Content>

<asp:Content ContentPlaceHolderID="mysampleCode" runat="server">
    <div class="col-md-12">
    <h2>My Sample Code</h2>
    <pre>
    var objectName = {}; // SYNTAX FOR JAVASCRIPT OBJECT

    // Now we will make an object called myObject

    var myObject = {
	    name: “Enrina”,
	    age: 19,
	    gender: Female
	    profile: function(){
                    alert( "Hi my name is " + myObject.name + ", " + myObject.gender + "and " + myObject.age + " of age!")
        }
    }

    //OUTPUT WILL BE ON ALERTBOX WITH CONCATENATED STRING
    myObject.profile();

    </pre>
    <img class="sample-img" src="images/popup.jpg" alt="image of an alert box">
    <p>The code above how to create an object in javascript. First we set the object name which is "myObject". Then we set the properties of our object which is "name", "age", "gender", and a method that will pop up an alert box with concatenated string.</p>
    </div>
</asp:Content>

<asp:Content ContentPlaceHolderID="SampleCode" runat="server">
    <div class="col-md-12 class=">
    <h2>In-Class Example</h2>
    <pre>
    var objectName = {}; // SYNTAX FOR JAVASCRIPT OBJECT

    // Now we will make an object called myObject

    var myCat = {
	    name: “Chester”,
	    age: 6,
	    breed: "Maine Coon"
	    purr: function(){
                    alert( "Prrrrrrrrrrrrrrrrrrrrrrrrr")
        }
    }

    //OUTPUT WILL BE ON ALERTBOX
    myCat.purr();

    </pre>
    <img class="sample-img" src="images/popup-sample.jpg" alt="image of an alert box">
    <p>The code above how to create an object in javascript. First we set the object name which is "myObject". Then we set the properties of our object which is "name", "age", "breed", and a method that will pop up an alert box if the method was called.</p> 
    <p> This is a class example from HTTP 5103 - Web Programming and the instructor Sean Doyle</p>
    </div>
</asp:Content>

<asp:Content ContentPlaceHolderID="usefulLinks" runat="server">
            <h2>Other Resources for this topic </h2>
            <ul class="usefulLinks">
                <li><h3><a href="https://www.w3schools.com/js/js_objects.asp" target="_blank">Javascript Object W3schools</a></h3></li>
                <li><h3><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Working_with_Objects" target="_blank">Working with Object</a></h3></li>
                <li><h3><a href="https://javascript.info/object" target="_blank">Objects</a></h3></li>
            </ul>
</asp:Content>
